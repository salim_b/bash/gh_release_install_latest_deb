# `gh_release_install_latest_deb`

***Deprecated*** in favor of the way more powerful [deb-get](https://github.com/wimpysworld/deb-get).

## See also

- [binenv](https://github.com/devops-works/binenv)
- [bin-get](https://github.com/OhMyMndy/bin-get)
- [gh-release-install](https://github.com/jooola/gh-release-install)
- [install-release](https://github.com/Rishang/install-release)

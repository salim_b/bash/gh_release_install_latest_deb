#!/bin/bash
#
# Download and install a GitHub repository's latest Debian package [release](https://docs.github.com/en/repositories/releasing-projects-on-github/about-releases) as conveniently as possible.
#
# Parameters:
#
# - `$1`: Name of the GitHub repository in `repos.toml`.
#
# Requirements:
#
# - CLI tools:
#   - [`dasel`](https://github.com/TomWright/dasel)
#   - [`gh`](https://cli.github.com/)
#   - [`pkexec`](https://www.freedesktop.org/software/polkit/docs/latest/pkexec.1.html) (when run without superuser privileges)
#
# - Config files:
#   - `repos.toml` with an entry for each GitHub repository setting the desired glob pattern (`gh release download --pattern`)

# determine directory from which the script is run
WD="${BASH_SOURCE%/*}"
if [[ ! -d "$WD" ]]; then WD="$PWD"; fi

# ensure first param is given
if [[ -z "$1" ]] ; then
  echo "No argument provided. You need to specify a name from \`repos.toml\` as first argument to this script."
  exit 1
fi

## check whether we are root
if [ "$EUID" -eq 0 ] ; then
  is_root="true"
else
  is_root="false"
fi

# ensure required tools are available
if ! command -v dasel >/dev/null ; then
  echo "\`dasel\` is required but not found on PATH. See https://daseldocs.tomwright.me/installation"
  exit 1
fi
if ! command -v gh >/dev/null ; then
  echo "\`gh\` is required but not found on PATH. See https://cli.github.com/"
  exit 1
fi
if ! command -v sd >/dev/null ; then
  echo "\`sd\` is required but not found on PATH. See https://github.com/chmln/sd#installation"
  exit 1
fi
## if `sudo` is available, check whether we have superuser privileges, and if not (and not already root), require pkexec
if command -v sudo >/dev/null && sudo -vn 2> /dev/null ; then
  has_sudo="true"
else
  has_sudo="false"
fi
if [[ $is_root == "false" ]] && [[ $has_sudo == "false" ]] && ! command -v pkexec >/dev/null ; then
  echo "\`pkexec\` is required but not found on PATH."
  exit 1
fi

# ensure config entry exists (only done for separate/better error msg))
if [[ $(dasel select --file="${WD}/repos.toml" --selector="${1}") == "" ]] ; then
  echo "No entry found in \`repos.toml\` for \"${1}\"."
  exit 1
fi

# read config entry
repo=$(dasel select --file="${WD}/repos.toml" --selector="${1}.repo" | sd "(^'|'$)" "")
pattern=$(dasel select --file="${WD}/repos.toml" --selector="${1}.pattern" | sd "(^'|'$)" "")

# ensure repo key is given
if [[ $repo == "" ]] ; then
  echo "No \`repo\` key found in \`repos.toml\` entry for ${1}."
  exit 1
fi

# ensure pattern key is given
if [[ $pattern == "" ]] ; then
  echo "No \`pattern\` key found in \`repos.toml\` entry for ${1}."
  exit 1
fi

# create temp dir and grant access to all users (incl. user '_apt')
tmp_dir=$(mktemp -p /tmp -d gh_release_install_latest_deb.XXXXXX)
chmod a+rx "${tmp_dir}"

# download release
gh release download --dir="${tmp_dir}" --repo="${repo}" --pattern="${pattern}"

# ensure exactly one DEB file was matched by pattern
deb_files=("${tmp_dir}"/*.deb)
n_debs=${#deb_files[@]}

if [[ n_debs -lt 1 ]] ; then
  echo "No DEB files found matching the \`pattern\` key in \`repos.toml\` entry for ${1}. Please fix the pattern and then run again."
  rm -rf "${tmp_dir}"
  exit 1
elif [[ n_debs -gt 1 ]] ; then
  echo "Multiple DEB files found matching the \`pattern\` key in \`repos.toml\` entry for ${1}. Please ensure the pattern matches a single DEB file only and then run again."
  rm -rf "${tmp_dir}"
  exit 1
fi

# install release
if [[ $is_root == "true" ]] ; then
  apt install "${tmp_dir}"/*.deb
elif [[ $has_sudo == "true" ]] ; then
  sudo apt install "${tmp_dir}"/*.deb
else
  pkexec env DISPLAY="$DISPLAY" XAUTHORITY="$XAUTHORITY" bash -c "sudo apt install ${tmp_dir}/*.deb"
fi

# clean up
rm -rf "${tmp_dir}"
